<?php

// WikiBot by Paris Charilaou
// Task: add an empty editorial template to the Talk Pages of all articles that are:
// Criteria: not under construction, and have no copy-vio templates.

require("config.php");

//initialization
//ob_end_flush();
ob_start();

// josmart change
if( isset( $_GET['mode'] ) ) {
  $_GET['mode'] ? $mode = $_GET['mode'] : $mode = 'list'; # list or edit
  if ($mode != "list" && $mode != "edit") { $mode = 'list'; } #default to list if not correct mode
}
else {
  $mode = 'list';
}

if(php_sapi_name() === 'cli'){
 $mode = 'edit';
}


$cookie = '';
//$domain = "http://".$_SERVER['SERVER_ADDR'];
$format = 'xml';
$upperlimit = '500';
$header = array(
    "Content-type: application/x-www-form-urlencoded"
);

//user editable
$testedit = false; #keep false for production use
$firstletter = "o";
$age = 3; # in days
$detection_criteria = array(
"{{Under construction}}",
"{{Process | reason=copyvio",
"{{Process|reason=copyvio",
"{{Process |reason=copyvio",
"{{Process| reason=copyvio"
);
$detection_criteria_talk = array(
    "{{Editorial process |",
    "{{Editorial process|",
    "{{Editorial process| ",
    "{{Editorial process | ",
    "{{Editorial process"
);

$prepend = "{{Editorial process
|sources
|citation
|breakdown_headings
|categories_portals
|synonyms
|images_licenses
|wikilinks
|grammar_typography
}}\n\n";


$summary = "wikibot test edit"; #"Editorial Template insertion";

//dont edit from here onwards
$version = 0.9;
$token = '';
$loggedin = false;
$prepend = urlencode($prepend);
//-----------------------

function curlz($url,$post=false) {
    // create a new cURL resource
    //echo "Running curl...<br/>";
    global $cookie,$token,$version,$header;
    $postfields = '';
    if ($post) {
        $parsedurl = parse_url($url);
        $postfields = $parsedurl['query'];   
    }
    $ch = curl_init();
    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, $post);
    if ($post) { curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields); }
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch, CURLOPT_COOKIEJAR, 'wikicookies.txt');
    //curl_setopt($ch, CURLOPT_COOKIEFILE, 'wikicookies.txt');
    if ($cookie != '') { curl_setopt($ch, CURLOPT_COOKIE, $cookie); }
    //echo "<br/>CURL cookie: $cookie <br/>";
    curl_setopt($ch, CURLOPT_USERAGENT, "WikiBot/$version Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.220 Safari/535.1");
    // grab URL and pass it to the browser
    $output = curl_exec($ch);
    //echo $output;
    // close cURL resource, and free up system resources
    curl_close($ch);
    //echo $output;
    //$outputz = simplexml_load_string($output);
    return $output;
}


//login

$todo = "$domain/api.php?format=$format&action=login&lgname=$username&lgpassword=$password";

$xml = curlz($todo,true);

$dataz = xml2array($xml);
/*echo "<pre>";
print_r($dataz);
echo "</pre>";*/
$token = $dataz['api']['login_attr']['token'];
$result = $dataz['api']['login_attr']['result'];
$prefix = $dataz['api']['login_attr']['cookieprefix'];
$cookie = $dataz['api']['login_attr']['cookieprefix']."_session=".$dataz['api']['login_attr']['sessionid'];
//echo "Cookie sent: $cookie <br/><br/>";
if ($result == 'NeedToken') {
    $todo .= "&lgtoken=$token";
    //echo "<br/>Continuing login with: $todo <br/>";
}
$xml = curlz($todo,true);
$dataz = xml2array($xml);
/*echo "<pre>";
print_r($dataz);
echo "</pre>"; */

$result = $dataz['api']['login_attr']['result'];
if ($result != "success") {
    $cookieuser = $dataz['api']['login_attr']['lgusername'];
    $cookieuserid = $dataz['api']['login_attr']['lguserid'];
    $cookietoken = $dataz['api']['login_attr']['lgtoken'];
    //$token = $cookietoken;
    $cookie .= ";".$prefix."Username=$cookieuser;".$prefix."UserID=$cookieuserid;".$prefix."Token=$cookietoken" ;
    echo "<br/>WIKIBOT LOG IN SUCCESS!<br/>"; // New cookie: $cookie<br/>";
    ob_end_flush();
    flush();
    $loggedin = true;
}

if ($loggedin) {
    echo "<hr />";    
    //=====
    $todo = "$domain/api.php?format=$format&action=query&prop=info|revisions&intoken=edit&titles=Main_Page";
    $xmledit = curlz($todo,true);
    $dataedit = xml2array($xmledit);
    $token_edit = urlencode($dataedit['api']['query']['pages']['page_attr']['edittoken']);
    //echo "<br/>Token edit: $token_edit<br/>";
    /*
    $todo = "$domain/api.php?format=$format&action=edit&title=Talk:The_Development_of_the_Ear&summary=$summary&token=$token_edit&prependtext=$prepend";
    $xmledit = curlz($todo,true);
    echo $xmledit;
    $dataedit = xml2array($xmledit);
    echo "<pre>";
    print_r($dataedit);
    echo "</pre>";
    exit();
    */
    //=====
    
    //get all articles list
    unset($articles);
    $articles = array();
    ob_start();
    echo "<br/>Getting all articles...<br/>";
    $morepages = false;
    $count = 1;
    do {
        //echo "<br/>Count: $count";
        $apfrom ='';
        if ($count > 1 && $morepages != '') {
            $apfrom = $morepages;
        }
        unset($morepages);
        $todo = "$domain/api.php?action=query&format=$format&list=allpages&apfilterredir=nonredirects&aplimit=$upperlimit&apprefix=&apfrom=$apfrom";
        //echo "TODO: $todo <br/>";
        $xml = curlz($todo);
        //echo $xml;
        $dataz = xml2array($xml);
        //echo "<pre>".print_r($dataz,true)."</pre>";
        $newlist = $dataz['api']['query']['allpages']['p'];
        $currentlist = array();
        foreach ($newlist as $k=>$v) {
            if (stripos($k,"_attr") !== false) { $currentlist[] = $v; }
        }
        /*echo "<pre>";
        if ($count >1) { print_r($newlist); }
        echo "</pre>";*/
        if (count($articles) == 0) { $articles = $currentlist; }
        else { $articles = array_merge($articles,$currentlist); }
        $morepages = isset($dataz['api']['query-continue']['allpages_attr']['apfrom']) ? $dataz['api']['query-continue']['allpages_attr']['apfrom'] : '';
        $morepages != '' ? $morepages = urlencode($morepages) : $morepages = '';
        /*echo "<pre>";
        print_r($dataz);
        echo "</pre>";*/
        $count++;
        //echo "morepages: ".$morepages."<br/>";
    } while ($morepages != '');
    //echo "<pre>";
    //print_r($articles);
    echo "Found total: ".count($articles)."<br/><br/><hr />";
    $titles = gettitles($articles);
    ob_end_flush();
    flush();
    //print_r(gettitles($articles));
    //echo "</pre>";
    
    //get each article and compile compatible list for editing
    $tocheck = array();
    $edited = array();
    $newpages = array();
    $timestarted = microtime(true);
    
    foreach ($titles as $key=>$val) {
        ob_start();
        $valz = urlencode(trim($val));
        $todo = "$domain/api.php?titles=$valz&format=$format&action=query&export&exportnowrap";
        //echo $todo;
        $xml = curlz($todo);
        $dataz = xml2array($xml);
        $titlezzz = $dataz['mediawiki']['page']['title'];
        $text = $dataz['mediawiki']['page']['revision']['text'];
        $oldbytes = $dataz['mediawiki']['page']['revision']['text_attr']['bytes'];
        $timestamp = $dataz['mediawiki']['page']['revision']['timestamp'];
        $today = time();
        $timestampz = strtotime($timestamp);
        $backer = strftime("%Y-%m-%d %H:%M:%S",$timestampz);
        //echo "<br/>Timestamp: $timestamp<br/>Timestampz: $timestampz <br/>Processed: $backer";
        $agez = $age * 24 * 60 * 60; #convert in seconds
        
        if ($today-$timestampz>$agez && multi_stripos($text,$detection_criteria,0,true) == 0 && $titlezzz != "Main Page") {
            //goto talk page
            $valzz = "Talk:$valz";
            //echo "checking talk page: ".$valzz."";
            $todo = "$domain/api.php?titles=$valzz&format=$format&action=query&export&exportnowrap";
            //echo "<br/>$todo";
            $xmlzz = curlz($todo);
            $datazz = xml2array($xmlzz);
            //if (strtolower(substr($valz,0,1)) == strtolower($firstletter)) { echo "<pre>"; print_r($dataz); echo "</pre>"; }

			// josmart: handling NOTICE undefined index 'page' 
            if(isset($datazz['mediawiki']['page'])) $textzz = $datazz['mediawiki']['page']['revision']['text'];
            else $textzz="";
            
            //echo "$val : ".multi_stripos($textzz,$detection_criteria_talk,0,true)."<br/>";
            if (multi_stripos($textzz,$detection_criteria_talk,0,true) == 0) {
                //echo "...editorial template <b>missing</b>...<br/>";//no editorial template
                $tocheck[] = $val;
                if ($mode == "list") {
                    if ($testedit === true) {
                        //echo "First letter: ".strtolower(substr($val,0,1));
                        if (strtolower(substr($val,0,1)) == strtolower($firstletter)) {
                            echo "entering TEST-edit mode for: $val .......";
                            $titlez = urlencode("Talk:$val");
                            $now = strftime("%Y%m%d%H%M%S",time());
                            $todo = "$domain/api.php?format=$format&bot&minor&action=edit&title=$titlez&summary=$summary&token=$token_edit&prependtext=$prepend";
                            $xmledit = curlz($todo,true);
                            $dataedit = xml2array($xmledit);
                            echo "<pre>";
                            print_r($dataedit);
                            echo "</pre>";
                            $editresult = $dataedit['api']['edit_attr']['result'];
                            $editoldrev = $dataedit['api']['edit_attr']['oldrevid'];
                            $editrevid = $dataedit['api']['edit_attr']['newrevid'];
                            $erroredit = "(".$dataedit['api']['error_attr']['code'].") :".$dataedit['api']['error_attr']['info'];
                            if (strtolower($editresult) == "success") {
                                //echo "SUCCESS (new revid: $editrevid)<br/>";
                                $edited[] = $val;
                                if ($editoldrev == 0) { $newpages[] = $val; }
                            }
                            else {
                                echo "Error: <i>`$erroredit`</i><br/>";
                            }
                        }
                    }
                }
                elseif ($mode == "edit") {
                    //edit code
                    echo "Trying to edit: $val .......";
                    $now = strftime("%Y%m%d%H%M%S",time());
                    $titlez = urlencode("Talk:$val");
                    $todo = "$domain/api.php?format=$format&action=edit&bot&minor&title=$titlez&summary=$summary&token=$token_edit&prependtext=$prepend";
                    $xmledit = curlz($todo,true);
                    $dataedit = xml2array($xmledit);
                    /*echo "<pre>";
                    print_r($dataedit);
                    echo "</pre>";*/
                    
                    // josmart change
                    if( isset($dataedit['api']['error_attr']) ) {
                      $editresult = $dataedit['api']['edit_attr']['result'];
                      $editrevid = $dataedit['api']['edit_attr']['newrevid'];
                      $editoldrev = $dataedit['api']['edit_attr']['oldrevid'];
                      $errorcode = $dataedit['api']['error_attr']['code'];
                    }
                    else {
                        $editresult = 0;
                        $editrevid = 0;
                        $editoldrev = 0;
						$errorcode = 0;
                    }
                    $erroredit = "(".$dataedit['api']['error_attr']['code'].") :".$dataedit['api']['error_attr']['info'];
                    if (strtolower($editresult) == "success") {
                        echo "SUCCESS (new revid: $editrevid)<br/>";
                        $edited[] = $val;
                        if ($editoldrev == 0) { $newpages[] = $val; }
                    }
                    else {
                        echo "Error: <i>`$erroredit`</i> ($val)<br/>";
                    }
                    
                }
            }
            else {
                //echo "...editorial template DETECTED!<br/>";
            }
            /*echo "<pre>";
            print_r($datazz);
            echo "</pre>";*/
        }
        /*echo "<pre>";
        print_r($dataz);
        echo "</pre>";*/
        ob_end_flush();
        flush();
    }
    $timefinished = microtime(true);
    $timetaken = $timefinished-$timestarted;
    //echo "debug: timefinished: $timefinished, timestarted: $timestarted, timetaken: $timetaken<br/>";
    $timetaken = "<br/><br/>Time taken to complete: ".strftime("%M mins %S secs",$timetaken);
    if ($mode == 'edit') {
        echo "<br/><br/>Added editorial template to ".count($edited)." talk pages:<br/>";
        echo "<ol>";
        foreach ($edited as $kk=>$vv) {
            echo "<li><a href='$domain/index.php/Talk:$vv' target=_blank>Talk:$vv</a></li>";
        }
        echo "</ol><br/>";
        if (count($edited) > 0) { echo "<b>NOTE: ".count($newpages)." out of the ".count($edited)." did not exist and they have been just created!</b>"; }
        echo $timetaken;
    }
    else {
        //echo "entered mode: list";
        if ($testedit === true) {
            echo "<br/><br/><b>WARNING:</b> This is a testedit mode, modifying only articles starting with letter: <b>$firstletter</b> <br/>";
            echo "Added editorial template to ".count($edited)." talk pages:<br/>";
            echo "<ol>";
            foreach ($edited as $kk=>$vv) {
                echo "<li><a href='$domain/index.php/Talk:$vv' target=_blank>Talk:$vv</a></li>";
            }
            echo "</ol><br/>";
            if (count($edited) > 0) { echo "<b>NOTE: ".count($newpages)." out of the ".count($edited)." did not exist and they have been just created!</b>"; }
        }
        echo "<br/><br/>There are ".count($tocheck)." talk pages that need to be added an editorial template:<br/>";
        echo "<ol>";
        foreach ($tocheck as $kk=>$vv) {
            echo "<li><a href='$domain/index.php/Talk:$vv' target=_blank>Talk:$vv</a></li>";
        }
        echo "</ol>";
        echo $timetaken;
    }
    /*echo "<pre>";
    print_r($tocheck);
    echo "</pre>";*/
    exit();
}

// helper functions

function multi_stripos($haystack, $needles, $offset=0,$total=false) {
    if( is_array($haystack) ) return 0;	// josmart change
    $count = 0;
    foreach($needles as $k=>$v) {
        $found[$v] = stripos($haystack, $v, $offset);
        if ($found[$v] !== false) { $count++; }
    }
    if ($total !== false) { return $count; }
    else { return $found; }
}

function gettitles($list) {
    $titles = array();
    foreach ($list as $key=>$val) {
       // if (stripos($key,"_attr") !== false) {
            //echo "<br/>getting attribute...".$val['title'];
            $titles[] = $val['title'];
       // }
    }
    return $titles;
}

function xml2array($contents, $get_attributes=1, $priority = 'tag') { 
    if(!$contents) return array(); 

    if(!function_exists('xml_parser_create')) { 
        //print "'xml_parser_create()' function not found!"; 
        return array(); 
    } 

    //Get the XML parser of PHP - PHP must have this module for the parser to work 
    $parser = xml_parser_create(''); 
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss 
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); 
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1); 
    xml_parse_into_struct($parser, trim($contents), $xml_values); 
    xml_parser_free($parser); 

    if(!$xml_values) return;//Hmm... 

    //Initializations 
    $xml_array = array(); 
    $parents = array(); 
    $opened_tags = array(); 
    $arr = array(); 

    $current = &$xml_array; //Refference 

    //Go through the tags. 
    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array 
    foreach($xml_values as $data) { 
        unset($attributes,$value);//Remove existing values, or there will be trouble 

        //This command will extract these variables into the foreach scope 
        // tag(string), type(string), level(int), attributes(array). 
        extract($data);//We could use the array by itself, but this cooler. 

        $result = array(); 
        $attributes_data = array(); 
         
        if(isset($value)) { 
            if($priority == 'tag') $result = $value; 
            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
        } 

        //Set the attributes too. 
        if(isset($attributes) and $get_attributes) { 
            foreach($attributes as $attr => $val) { 
                if($priority == 'tag') $attributes_data[$attr] = $val; 
                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
            } 
        } 

        //See tag status and do the needed. 
        if($type == "open") {//The starting of the tag '<tag>' 
            $parent[$level-1] = &$current; 
            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
                $current[$tag] = $result; 
                if($attributes_data) $current[$tag. '_attr'] = $attributes_data; 
                $repeated_tag_index[$tag.'_'.$level] = 1; 

                $current = &$current[$tag]; 

            } else { //There was another element with the same tag name 

                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array 
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result; 
                    $repeated_tag_index[$tag.'_'.$level]++; 
                } else {//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag.'_'.$level] = 2; 
                     
                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag.'_attr']; 
                        unset($current[$tag.'_attr']); 
                    } 

                } 
                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1; 
                $current = &$current[$tag][$last_item_index]; 
            } 

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
            //See if the key is already taken. 
            if(!isset($current[$tag])) { //New Key 
                $current[$tag] = $result; 
                $repeated_tag_index[$tag.'_'.$level] = 1; 
                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data; 

            } else { //If taken, put all things inside a list(array) 
                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                    // ...push the new element into that array. 
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result; 
                     
                    if($priority == 'tag' and $get_attributes and $attributes_data) { 
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data; 
                    } 
                    $repeated_tag_index[$tag.'_'.$level]++; 

                } else { //If it is not an array... 
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag.'_'.$level] = 1; 
                    if($priority == 'tag' and $get_attributes) { 
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                             
                            $current[$tag]['0_attr'] = $current[$tag.'_attr']; 
                            unset($current[$tag.'_attr']); 
                        } 
                         
                        if($attributes_data) { 
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data; 
                        } 
                    } 
                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken 
                } 
            } 

        } elseif($type == 'close') { //End of tag '</tag>' 
            $current = &$parent[$level-1]; 
        } 
    } 
     
    return($xml_array); 
}  

?>