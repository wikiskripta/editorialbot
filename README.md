# EditorialBot

Bot created by Paris Charilaou for WikiLectures

## Description

* Version 1.0
* Doesn't work


## Installation

* Download and place the files to /extensions/Bots/EditorialBot/ folder.
* Rename _config.sample.php_ to _config.php_ and set variables.

## Authors and license

* Paris Charilaou, [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 First Faculty of Medicine, Charles University
